# Differences between NetLogo and PyWorlds

## Nomenclature

In NetLogo, the world is made up of *agents*. There are four types of agents: *turtles, patches, links*, and the *observer*.

PyWorlds uses a different nomenclature.
*agents* are entitites which move around the map, and are the equivalent of NetLogo turtles.
*tiles* are locations on the map, and are the equivalent of NetLogo patches.
*links* are equivalent to NetLogo links.

## Sets of agents

In NetLogo, you can access a set of agents (turtles, patches, or links) by using primitives that return *agentsets*. For example, you could issue commands like:

```
ask turtles [ fd 1 ]
ask patches [ set pcolor red]
``` 

In PyWorlds there are no special primitives to return sets of objects. Instead, you simply use Python's lists:

```
for agent in sim.agents:
    agent.fd(1)
for tile in sim.tiles:
    tile.color = "red"
```

## Instructing a random agent or tile

In NetLogo, you can send instructions to a random turtle or patch as follows:
```
ask one-of turtles [ set color green ]
ask one-of patches [ sprout 1 ]
```

In PyWorlds, you can use the Simulation.random_agent() or Simulation.random_tile() methods to send instructions:
```
sim.random_agent().color = "green"
sim.random_tile().sprout(1)
```

Decisions to make:
- what format of color to store
- nomencalture for color conversions
- dictionaries or lists for agents