import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyworlds",
    version="0.0.1",
    author="Mark Jacobsen",
    author_email="abu.isaiah@gmail.com",
    description="An agent-based modeling framework for Python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jacobsenmd/pyworlds",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Development Status :: 2 - Pre-Alpha"
    ],
    install_requires=[
        'numpy',
        'PyQt5',
        'PyOpenGL',
        'PyOpenGL_accelerate',
        'Pillow'
    ],
    python_requires='>=3.6',
)
