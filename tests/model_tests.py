import unittest

from pyworlds.model import *



# Here's our "unit tests".
class modelTests(unittest.TestCase):

    def test_size(self):
        self.sim = Model(10, 10)
        self.failUnless(self.sim.width == 10)
        self.failUnless(self.sim.height == 10)

    def test_coordinate_boundaries(self):
        # Test with horizontal and vertical wrapping
        self.sim = Model(10,10)
        self.sim.wrap_vertical = True
        self.sim.wrap_horizontal = True
        self.failUnless(self.sim.get_wrapped_coordinate(0, 0) == (0, 0))
        self.failUnless(self.sim.get_wrapped_coordinate(9, 8) == (9, 8))
        self.failUnless(self.sim.get_wrapped_coordinate(11, 12) == (1, 2))
        self.failUnless(self.sim.get_wrapped_coordinate(-1, -2) == (9, 8))
        self.failUnless(self.sim.get_wrapped_coordinate(-9, -8) == (1, 2))

        # Test with just horizontal wrapping (vertical constrained 0-9)
        self.sim = Model(10, 10)
        self.sim.wrap_vertical = False
        self.sim.wrap_horizontal = True
        self.failUnless(self.sim.get_wrapped_coordinate(0, 0) == (0, 0))
        self.failUnless(self.sim.get_wrapped_coordinate(9, 8) == (9, 8))
        self.failUnless(self.sim.get_wrapped_coordinate(11, 12) == (1,9))
        self.failUnless(self.sim.get_wrapped_coordinate(-1, -2) == (9, 0))
        self.failUnless(self.sim.get_wrapped_coordinate(-9, -8) == (1, 0))

        # Test with just vertical wrapping (horizontal constrained 0-9)
        self.sim = Model(10, 10)
        self.sim.wrap_vertical = True
        self.sim.wrap_horizontal = False
        self.failUnless(self.sim.get_wrapped_coordinate(0, 0) == (0, 0))
        self.failUnless(self.sim.get_wrapped_coordinate(9, 8) == (9, 8))
        self.failUnless(self.sim.get_wrapped_coordinate(11, 12) == (9, 2))
        self.failUnless(self.sim.get_wrapped_coordinate(-1, -2) == (0, 8))
        self.failUnless(self.sim.get_wrapped_coordinate(-9, -8) == (0, 2))

    def test_basic_movement(self):

        model = Model(20,20)
        a = Agent(model)

        a.x = 3
        a.y = 5
        a.set_heading(0)
        a.forward(1)
        self.failUnless(a.x == 3 and a.y == 4)

        a.set_heading(360) # Checks degree wrapping
        a.forward(1)
        self.failUnless(a.x == 3 and a.y == 3)

        a.right(90)
        a.forward(1)
        self.failUnless(a.x == 4 and a.y == 3)

        a.left(180)
        a.forward(1)
        self.failUnless(a.x == 3 and a.y == 3)

        a.right(45)
        a.forward(2)
        self.failUnless(round(a.x, 1) == 1.6 and round(a.y, 1) == 1.6)

        a.back(2)
        self.failUnless(a.x == 3 and a.y == 3)

        a.set_xy(5, 5)
        a.set_heading(90)

        a.forward(20)

        self.failUnless(abs(a.x -5) < 0.001 and abs(a.y - 5) < 0.001)

        a.set_xy(20,21)
        self.failUnless(a.x == 0 and a.y==1)

        a.set_xy(-19,-18)
        self.failUnless(a.x == 1 and a.y==2)

    def test_agent_measures(self):
        model = Model(20, 20)
        a1 = Agent(model)
        a1.set_xy(5,5)

        # test all 4 quadrants
        # we want to be within 2 degrees, because rounding errors may cause
        # degree to fluctuate by 1
        self.failUnless(abs(a1.get_heading_to_xy(4, 4) - 315) < 2)
        self.failUnless(abs(a1.get_heading_to_xy(6, 4) - 45) < 2)
        self.failUnless(abs(a1.get_heading_to_xy(6, 6) - 135) < 2)
        self.failUnless(abs(a1.get_heading_to_xy(4, 6) - 225) < 2)

        a2 = Agent(model)
        a2.set_xy(4, 4)
        self.failUnless(abs(a1.get_heading_to(a2) - 315) < 2)
        self.failUnless(abs(a1.get_range_to(a2) - 1.414) < 0.01)

        t = model.tiles[4][4]
        self.failUnless(abs(a1.get_heading_to(t) - 315) < 2)
        self.failUnless(abs(a1.get_range_to(t) - 1.414) < 0.01)

    def test_tile_measures(self):
        model = Model(20, 20)
        t1 = model.tiles[5][5]
        t2 = model.tiles[4][4]

        self.failUnless(abs(t1.get_heading_to_xy(4, 4) - 315) < 2)
        self.failUnless(abs(t1.get_heading_to_xy(6, 4) - 45) < 2)
        self.failUnless(abs(t1.get_heading_to_xy(6, 6) - 135) < 2)
        self.failUnless(abs(t1.get_heading_to_xy(4, 6) - 225) < 2)

        self.failUnless(abs(t1.get_heading_to(t2) - 315) < 2)
        self.failUnless(abs(t1.get_range_to(t2) - 1.414) < 0.01)

    def test_face(self):
        model = Model()
        a1 = Agent(model)
        a1.set_xy(5,5)

        # test all 4 quadrants
        # we want to be within 2 degrees, because rounding errors may cause
        # degree to fluctuate by 1
        self.failUnless(abs(a1.face_xy(4, 4) - 315) < 2)
        self.failUnless(abs(a1.face_xy(6, 4) - 45) < 2)
        self.failUnless(abs(a1.face_xy(6, 6) - 135) < 2)
        self.failUnless(abs(a1.face_xy(4, 6) - 225) < 2)

        a2 = Agent(model)
        a2.set_xy(4, 4)
        self.failUnless(abs(a1.face(a2) - 315) < 2)

        t = model.tiles[4][4]
        self.failUnless(abs(a1.face(t) - 315) < 2)

    def test_move_to(self):
        model = Model()
        a1 = Agent(model)
        a1.move_to(model.tiles[6][6])
        self.failUnless(a1.x == 6 and a1.y == 6)

        a2 = Agent(model)
        a2.move_to(a1)
        self.failUnless(a2.x == 6 and a2.y == 6)


    def testVonNeumannNeighborhood(self):
        # test in the middle of the board

        # this is what the von Neumann neighborhood should return
        model = Model()
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[4][5])
        correctNeighborhood.append(model.tiles[6][5])
        correctNeighborhood.append(model.tiles[5][4])
        correctNeighborhood.append(model.tiles[5][6])

        neighborhood = model.tiles[5][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

    def testVonNeumannEdgesBox(self):
        # test each edge of the von Neumann neighborhood with no wrapping

        model = Model(10,10)
        model.wrap_vertical = False
        model.wrap_horizontal = False


        # Top edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[0][4])
        correctNeighborhood.append(model.tiles[0][6])
        correctNeighborhood.append(model.tiles[1][5])
        neighborhood = model.tiles[0][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))


        # Bottom edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[8][5])
        correctNeighborhood.append(model.tiles[9][4])
        correctNeighborhood.append(model.tiles[9][6])
        neighborhood = model.tiles[9][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Left edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][1])
        correctNeighborhood.append(model.tiles[4][0])
        correctNeighborhood.append(model.tiles[6][0])
        neighborhood = model.tiles[5][0].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Right edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][8])
        correctNeighborhood.append(model.tiles[4][9])
        correctNeighborhood.append(model.tiles[6][9])
        neighborhood = model.tiles[5][9].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

    def testVonNeumannToroid(self):

        model = Model(10, 10)
        model.wrap_vertical = True
        model.wrap_horizontal = True

        # Top edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[0][4])
        correctNeighborhood.append(model.tiles[0][6])
        correctNeighborhood.append(model.tiles[1][5])
        correctNeighborhood.append(model.tiles[9][5])
        neighborhood = model.tiles[0][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Bottom edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[8][5])
        correctNeighborhood.append(model.tiles[9][4])
        correctNeighborhood.append(model.tiles[9][6])
        correctNeighborhood.append(model.tiles[0][5])
        neighborhood = model.tiles[9][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Left edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][1])
        correctNeighborhood.append(model.tiles[4][0])
        correctNeighborhood.append(model.tiles[6][0])
        correctNeighborhood.append(model.tiles[5][9])
        neighborhood = model.tiles[5][0].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Right edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][8])
        correctNeighborhood.append(model.tiles[4][9])
        correctNeighborhood.append(model.tiles[6][9])
        correctNeighborhood.append(model.tiles[5][0])
        neighborhood = model.tiles[5][9].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

    def test_vn_horizontalwrap(self):
        # test each edge of the von Neumann neighborhood with no wrapping

        model = Model(10, 10)
        model.wrap_vertical = False
        model.wrap_horizontal = True


        # Top edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[0][4])
        correctNeighborhood.append(model.tiles[0][6])
        correctNeighborhood.append(model.tiles[1][5])
        neighborhood = model.tiles[0][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Bottom edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[8][5])
        correctNeighborhood.append(model.tiles[9][4])
        correctNeighborhood.append(model.tiles[9][6])
        neighborhood = model.tiles[9][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Left edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][1])
        correctNeighborhood.append(model.tiles[4][0])
        correctNeighborhood.append(model.tiles[6][0])
        correctNeighborhood.append(model.tiles[5][9])
        neighborhood = model.tiles[5][0].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Right edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][8])
        correctNeighborhood.append(model.tiles[4][9])
        correctNeighborhood.append(model.tiles[6][9])
        correctNeighborhood.append(model.tiles[5][0])
        neighborhood = model.tiles[5][9].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

    def testVonNeumannCenter(self):
        model = Model(10, 10)
        neighborhood = model.tiles[5][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, True)
        self.failUnless(model.tiles[5][5] in neighborhood)

    def test_vn_verticalwrap(self):
        # test each edge of the von Neumann neighborhood with no wrapping

        model = Model(10, 10)
        model.wrap_vertical = True
        model.wrap_horizontal = False

        # Top edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[0][4])
        correctNeighborhood.append(model.tiles[0][6])
        correctNeighborhood.append(model.tiles[1][5])
        correctNeighborhood.append(model.tiles[9][5])
        neighborhood = model.tiles[0][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Bottom edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[8][5])
        correctNeighborhood.append(model.tiles[9][4])
        correctNeighborhood.append(model.tiles[9][6])
        correctNeighborhood.append(model.tiles[0][5])
        neighborhood = model.tiles[9][5].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Left edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][1])
        correctNeighborhood.append(model.tiles[4][0])
        correctNeighborhood.append(model.tiles[6][0])
        neighborhood = model.tiles[5][0].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

        # Right edge
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[5][8])
        correctNeighborhood.append(model.tiles[4][9])
        correctNeighborhood.append(model.tiles[6][9])
        neighborhood = model.tiles[5][9].get_neighboring_tiles(NEIGHBORHOOD_VONNEUMANN, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

    def testMooreNeighborhood(self):
        # test in the middle of the board
        model = Model(10, 10)
        model.wrap_vertical = False
        model.wrap_horizontal = False

        # this is what the Moore neighborhood should return
        correctNeighborhood = []
        correctNeighborhood.append(model.tiles[4][5])
        correctNeighborhood.append(model.tiles[6][5])
        correctNeighborhood.append(model.tiles[5][4])
        correctNeighborhood.append(model.tiles[5][6])

        correctNeighborhood.append(model.tiles[4][4])
        correctNeighborhood.append(model.tiles[6][6])
        correctNeighborhood.append(model.tiles[4][6])
        correctNeighborhood.append(model.tiles[6][4])

        neighborhood = model.tiles[5][5].get_neighboring_tiles(NEIGHBORHOOD_MOORE, False)
        self.failUnless(set(correctNeighborhood) == set(neighborhood))

    def test_xy_consistency(self):
        # Added this test because of confusion about the fact that Y comes before X in the
        # nested dictionary ordering
        model = Model()
        self.failUnless(model.tiles[3][5].x == 5 and model.tiles[3][5].y == 3)