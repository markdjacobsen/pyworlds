#!/usr/bin/python

import sys
from ui import *
from model import *
from PyQt5 import QtGui
import os

def main():

    app = QtGui.QApplication(sys.argv)
    #app.setOrganizationName("Uplift Aeronautics")
    #app.setOrganizationDomain("uplift.aero")
    app.setApplicationName("PyWorlds")
    #app.setWindowIcon(QIcon(state.config.settings['appicon']))
    window = MainWindow()
    sys.exit(app.exec_())



__version__ = '0.0.1'
gcsdir = os.path.dirname(__file__)
if __name__ == '__main__':
    main()