from pyworlds.model import *
from pyworlds.ui import *

class GameOfLife (Model):

    def setup(self):
        print('GameOfLife.setup()')
        for y in self.tiles.keys():
            for x in self.tiles[y].keys():
                self.tiles[y][x].alive = random.randint(0,1)
                if self.tiles[y][x].alive == 1:
                    self.tiles[y][x].color = (255,255,255)
                else:
                    self.tiles[y][x].color = (0,0,0)

    def tick_override(self):

        for t in self.all_tiles.values():
            living_neighbors = 0
            neighborhood = t.get_neighboring_tiles(NEIGHBORHOOD_MOORE)
            for neighbor in neighborhood:
                if neighbor.alive:
                    living_neighbors += 1
            t.living_neighbors = living_neighbors

        for t in self.all_tiles.values():

            # Rule 1: die from underpopulation
            if t.living_neighbors < 2:
                t.alive = False

            # Rule 2: live on in moderation
            elif t.alive and (t.living_neighbors == 2 or t.living_neighbors == 3):
                t.alive = True

            # Rule 3: die from overpopulation
            elif t.living_neighbors > 3:
                t.alive = False

            # Rule 4: reproduce
            elif t.alive == False and t.living_neighbors == 3:
                t.alive = True

            # Living cells should turn white, dead cells turn black
            if t.alive == True:
                t.color = (255,255,255)
            else:
                t.color = (0,0,0)


mode = 1
g = GameOfLife(40,40)

if mode == 1:
    run_simulation(g)
else:
    g.reset()
    for i in range(1,100000):
        g.tick()
        if g.ticks % 10 == 0:
            print(g.ticks)