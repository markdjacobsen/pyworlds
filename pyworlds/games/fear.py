from pyworlds.model import *
from pyworlds.ui import *
import random

class MyAgent (Agent):
    def __init__(self):
        self.identity = []
        self.aggression = 0


class GameOfFear (Model):

    def setup(self):

        self.parameters['identity_dimensions'] = 3

        for y in self.tiles.keys():
            for x in self.tiles[y].keys():
                self.tiles[y][x].identity = random.sample(range(1, 2), self.parameters['identity_dimensions'])
                if self.tiles[y][x].alive == 1:
                    self.tiles[y][x].color = (255,255,255)
                else:
                    self.tiles[y][x].color = (0,0,0)

    def tick(self):

        for t in self.all_tiles.itervalues():
            living_neighbors = 0
            neighborhood = t.get_neighboring_tiles(NEIGHBORHOOD_MOORE)
            for neighbor in neighborhood:
                if neighbor.alive:
                    living_neighbors += 1
            t.living_neighbors = living_neighbors

        for t in self.all_tiles.itervalues():

            # Rule 1: die from underpopulation
            if t.living_neighbors < 2:
                t.alive = False

            # Rule 2: live on in moderation
            elif t.alive and (t.living_neighbors == 2 or t.living_neighbors == 3):
                t.alive = True

            # Rule 3: die from overpopulation
            elif t.living_neighbors > 3:
                t.alive = False

            # Rule 4: reproduce
            elif t.alive == False and t.living_neighbors == 3:
                t.alive = True

            # Living cells should turn white, dead cells turn black
            if t.alive == True:
                t.color = (255,255,255)
            else:
                t.color = (0,0,0)


mode = 0
g = GameOfFear(40,40)

if mode == 0:
    run_simulation(g)
else:
    g.reset()
    for i in range(1,1000):
        g._tick()
        if g.ticks % 10 == 0:
            print(g.ticks)