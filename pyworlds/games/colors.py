from pyworlds.model import *
from pyworlds.ui import *

class Colors (Model):

    def setup(self):
        print('GameOfLife.setup()')
        for y in self.tiles.keys():
            for x in self.tiles[y].keys():
                self.tiles[y][x].color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))


    def tick_override(self):

        #for t in self.all_tiles.values():
        #    t.color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

        for key, val in self.all_tiles.items():
            val.color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))


mode = 1
g = Colors(40,40)

if mode == 1:
    run_simulation(g)
else:
    g.reset()
    for i in range(1,100000):
        g.tick()
        if g.ticks % 10 == 0:
            print(g.ticks)