import random
import math

NEIGHBORHOOD_VONNEUMANN = 0
NEIGHBORHOOD_MOORE = 1

COLORLIST_TERMINAL = ('#000000','#800000','#008000','#808000','#000080','#800080','#008080','#c0c0c0',
          '#808080','#ff0000','#00ff00','#ffff00','#0000ff','#ff00ff','#00ffff','#ffffff')
COLORLIST_TOPOGRAPHIC = ('#87cefa', '#7fff00', '#9acd32', '#006400', '#eee8aa', '#ffff00', '#ffa500', '#ff0000', '#a52a2a')

class Colors:
    BLACK = 0
    RED = 1
    GREEN = 2
    YELLOW = 3
    BLUE = 4
    MAGENTA = 5
    CYAN = 6
    LIGHT_GRAY = 7
    DARK_GRAY = 8
    BRIGHT_RED = 9
    BRIGHT_GREEN = 10
    BRIGHT_YELLOW = 11
    BRIGHT_BLUE = 12
    BRIGHT_MAGENTA = 13
    BRIGHT_CYAN = 14
    WHITE = 15


class Model:

    def __init__(self, w = 20, h =20):

        self.width = w
        self.height = h
        self.tilesize = 16
        self.wrap_vertical = True
        self.wrap_horizontal = True
        self.tiles = {}
        self.all_tiles = {}
        self.parameters = {}
        self.ticks = 0

    def tick(self):
        # Advance the tick counter and call the user-overridable tick function
        self.ticks = self.ticks + 1
        self.tick_override()

    def agents_on(self):
        # TODO
        return



    def ca(self):
        self.clear_all()

    def cd(self):
        self.clear_drawing()

    def cag(self):
        self.clear_agents()

    def clear_agents(self):
        # TODO
        return

    def clear_all(self):
        # TODO
        return

    def clear_drawing(self):
        # TODO
        return

    def clear_patches(self):
        # TODO
        return

    def cp(self):
        self.clear_patches()

    def create_agents(self, num):
        # TODO implement
        # TODO figure out how to incorporate breeds
        return

    def move_to(self, x, y):
        # TODO implement
        return

    def random_agent(self):
        # TODO implement
        return

    def random_tile(self):
        # TODO implement
        return

    def reset_ticks(self):
        self.ticks = 0
        return

    def reset(self):

        self.ticks = 0
        i = 0

        self.tiles = {}
        for y in range(0, self.height):
            self.tiles[y] = {}
            for x in range(0, self.width):
                self.tiles[y][x] = Tile(self, x, y)
                self.all_tiles[i] = self.tiles[y][x]
                i += 1

        self.setup()

    def setup(self):
        print('Simulation.setup()')
        return


    def setxy(self, x, y):
        # TODO implement
        return

    def shuffled_agents(self):
        # TODO implement
        return

    def shuffled_tiles(self):
        # TODO implement
        return

    def tick_override(self):
        # To be overriden by the user
        return

    def tick_advance(self, amount):
        self.ticks = float(self.ticks) + amount
        # TODO implement
        return

    def get_wrapped_coordinate(self,x,y):
        """
        Wrap an out-of-bounds coordinate into a valid coordinate.

        Takes a potentially off-map coordinate and converts it to a
        wrapped coordinate, based on the topology of the world.
        :param x: raw X coordinate
        :param y: raw Y coordinate
        :return:

        Example:
        """
        newx = x
        newy = y
        # TODO Finish documentation
        # TODO Test
        # TODO Figure out what to do with invalid coordinates
        if self.wrap_vertical == True:
            while newy < 0:
                newy += self.height
            while newy >= self.height:
                newy -= self.height
        else:
            newy = max(newy, 0)
            newy = min(newy, self.height-1)

        if self.wrap_horizontal == True:
            while newx < 0:
                newx += self.width
            while newx >= self.width:
                newx -= self.width
        else:
            newx = max(newx, 0)
            newx = min(newx, self.width-1)

        return (newx,newy)




class Tile:
    def __init__(self, model, x, y):
        self.model = model
        self.color = '#000000'
        self.x = x
        self.y = y
        self.label = ""
        self.label_color = "red"

    def distance(self, obj):
        '''
        Returns the Euclidian distance to the specified object
        :param obj: An Agent subclass or Tile subclass
        :return:
        '''
        # TODO
        return

    def distancexy(self, x, y):
        '''
        Returns the Euclidian distance to the specified coordinates
        :param x: X coordinate
        :param y: Y coordinate
        :return: Euclidian distance
        '''
        # TODO
        return

    def get_heading_to(self, obj):
        """
        Get the heading to the specified agent

        :param obj: the target agent or tile

        Returns:
        :returns (int) degrees
        """

        return self.get_heading_to_xy(obj.x, obj.y)

    def get_heading_to_xy(self,x,y):
        # TODO: wrapped coordinates
        """
        Get the heading to the specified coordinate

        :param x (int): the target X coordinate
        :param y (int): the target Y coordinate

        Returns:
        :returns (int) degrees
        """

        # We reverse the equation for dy because screen coordinate Y axis runs bottom to top
        dx = x - self.x
        dy = self.y - y
        #range = math.sqrt(dx**2 + dy**2)
        #heading = math.floor(math.degrees(math.asin(dx / range)))
        theta_radians = math.atan2(dx, dy)
        heading = theta_radians * 180 / math.pi
        heading = constrain_heading(heading)
        return heading

    def get_neighboring_tiles(self, neighborhood_type=NEIGHBORHOOD_MOORE, includeCenter=False):
        """Returns a list of neighboring tiles.
        This function determines how many immediate neighbors an agent has
        First, generate a list of adjacent coordinates. There will either 4 or 8, depending on
        whether we are using Von Neumann space or Moore space
        """

        if neighborhood_type == NEIGHBORHOOD_VONNEUMANN:
            coords = [(self.x, self.y - 1),
                      (self.x, self.y + 1),
                      (self.x - 1, self.y),
                      (self.x + 1, self.y)]
        if neighborhood_type == NEIGHBORHOOD_MOORE:
            coords = [(self.x, self.y - 1),
                      (self.x, self.y + 1),
                      (self.x - 1, self.y),
                      (self.x + 1, self.y),
                      (self.x - 1, self.y - 1),
                      (self.x + 1, self.y - 1),
                      (self.x - 1, self.y + 1),
                      (self.x + 1, self.y + 1)]

        if includeCenter == True:
            coords.append((self.x, self.y))

        # Now conduct bounds check. coords_clean excludes any out-of-bounds and wraps tiles as necessary.
        for c in coords:
            if self.model.wrap_horizontal is True:
                if c[0] < 0:
                    coords.append((self.model.width - 1, c[1]))
                if c[0] >= self.model.width:
                    coords.append((0, c[1]))
            if self.model.wrap_vertical is True:
                if c[1] < 0:
                    coords.append((c[0], self.model.height - 1))
                if c[1] >= self.model.height:
                    coords.append((c[0], 0))

        tiles_list = []
        for c in coords:
            # 'coords' now includes both out-of-bounds coordinates and (for appropriate worlds) wrapped coordinates.
            # This next section creates a new list called tiles_list, but only copies valid coordinates over...
            # which excludes out-of-bounds coordinates
            #            try:
            #
            #                tiles_list.append(self.simulation.currentFrame.tiles[c[0]][c[1]])
            #                print c[0],c[1]
            #            except:
            #                continue
            #                print "excluding",c[0],c[1]

            if c[0] < 0 or c[0] >= self.model.width:
                continue
            if c[1] < 0 or c[1] >= self.model.height:
                continue
            tiles_list.append(self.model.tiles[c[1]][c[0]])

        return tiles_list

    def get_range_to(self,obj):
        """
        Get the range to the specified agent or tile

        :param obj): the target agent or tile

        Returns:
        :returns (float): range
        """
        return self.get_range_to_xy(obj.x,obj.y)

    def get_range_to_xy(self,x,y):
        # TODO: wrapped coordinates
        """
        Get the range to the specified coordinate

        :param x (int): the target X coordinate
        :param y (int): the target Y coordinate

        Returns:
        :returns (float): range
        """
        dx = self.x - x
        dy = self.y - y
        return math.sqrt(dx**2 + dy**2)

    def neighbors(self):
        # TODO
        return

    def neighbors4(self):
        # TODO
        return

    def sprout(self, num):
        # TODO implement
        return

class Agent:
    def __init__(self, model):
        self.model = model
        self.color = COLORLIST_TERMINAL[random.randint(0,15)]
        self.heading = 0.0
        self.hidden = False
        self.label = ""
        self.label_color = "white"
        self.pen_size = 1
        self.shape = "circle"
        self.size = 1
        self.x = 0.0
        self.y = 0.0

    def agents_here(self):
        # TODO incorporate breeds
        return

    def back(self, units):
        self.forward(-units)

    def bk(self, units):
        self.back(units)

    def can_move(self, distance):
        # TODO implement
        return

    def create_link_with(self, agent):
        # TODO
        return

    def die(self):
        # TODO implement
        return

    def distance(self, obj):
        '''
        Returns the Euclidian distance to the specified object
        :param obj: An Agent subclass or Tile subclass
        :return:
        '''
        # TODO
        return

    def distancexy(self, x, y):
        '''
        Returns the Euclidian distance to the specified coordinates
        :param x: X coordinate
        :param y: Y coordinate
        :return: Euclidian distance
        '''
    def downhill(self, attribute):
        # TODO
        return

    def downhill4(self, attribute):
        # TODO
        return

    def face(self, obj):
        '''
        Change heading to face the specified object.
        :param obj: agent or tile object
        :return: (it) the new heading in degrees
        '''
        return self.face_xy(obj.x, obj.y)

    def face_xy(self,x,y):
        """
        Change heading to face the specified coordinates

        :param x (int): the target X coordinate
        :param y (int): the target Y coordinate

        Returns:
        :returns (int) new heading in degrees
        """
        self.heading = self.get_heading_to_xy(x,y)
        return self.heading


    def fd(self, units):
        self.forward(units)
        return

    def forward(self, distance):
        dx = distance * math.sin(math.radians(self.heading))
        dy = distance * math.cos(math.radians(self.heading))
        self.x += dx
        self.y -= dy
        self.x, self.y = self.model.get_wrapped_coordinate(self.x, self.y)

        return

    def hatch(self, num):
        # TODO implement
        # TODO incorporate breeds
        return

    def get_heading_to(self, obj):
        """
        Get the heading to the specified agent

        :param obj: the target agent or tile

        Returns:
        :returns (int) degrees
        """

        return self.get_heading_to_xy(obj.x, obj.y)

    def get_heading_to_xy(self,x,y):
        # TODO: wrapped coordinates
        """
        Get the heading to the specified coordinate

        :param x (int): the target X coordinate
        :param y (int): the target Y coordinate

        Returns:
        :returns (int) degrees
        """

        # We reverse the equation for dy because screen coordinate Y axis runs bottom to top
        dx = x - self.x
        dy = self.y - y
        #range = math.sqrt(dx**2 + dy**2)
        #heading = math.floor(math.degrees(math.asin(dx / range)))
        theta_radians = math.atan2(dx, dy)
        heading = theta_radians * 180 / math.pi
        heading = constrain_heading(heading)
        return heading

    def get_range_to(self,obj):
        """
        Get the range to the specified agent or tile

        :param obj): the target agent or tile

        Returns:
        :returns (float): range
        """
        return self.get_range_to_xy(obj.x,obj.y)

    def get_range_to_xy(self,x,y):
        # TODO: wrapped coordinates
        """
        Get the range to the specified coordinate

        :param x (int): the target X coordinate
        :param y (int): the target Y coordinate

        Returns:
        :returns (float): range
        """
        dx = self.x - x
        dy = self.y - y
        return math.sqrt(dx**2 + dy**2)

    def hide(self):
        self.hidden = True

    def home(self):
        self.x = 0.0
        self.y = 0.0

    def jump(self, distance):
        # TODO figure out what NetLogo does differently with this command
        self.forward(distance)
        return

    def left(self, degrees):
        self.heading = constrain_heading(self.heading - degrees)

    def link_neighbor(self, agent):
        # TODO
        return

    def lt(self, degrees):
        self.left(degrees)
        return

    def move_to(self, obj):
        self.set_xy(obj.x, obj.y)
        return

    def my_in_links(self):
        return

    def my_links(self):
        return

    def my_out_links(self):
        # TODO
        return

    def patch_ahead(self):
        # TODO
        return

    def patch_at_heading_distance(self, heading, distance):
        # TODO
        return

    def patch_here(self):
        # TODO
        return

    def patch_left_and_ahead(self, degrees, distance):
        # TODO
        return

    def patch_right_and_ahead(self, degrees, distance):
        # TODO
        return

    def pd(self):
        self.pen_down()

    def pen_down(self):
        # TODO implement
        return

    def pen_erase(self):
        # TODO implement
        return

    def pen_up(self):
        # TODO implement
        return

    def pu(self):
        self.pen_up()

    def right(self, degrees):
        self.heading = constrain_heading(self.heading + degrees)
        return

    def rt(self, degrees):
        self.right(degrees)
        return

    def set_heading(self, heading):
        self.heading = constrain_heading(heading)

    def set_line_thickness(self, thickness):
        # TODO
        return

    def set_xy(self, x, y):
        self.x, self.y = self.model.get_wrapped_coordinate(x, y)

    def show(self):
        self.hidden = False

    def stamp(self):
        # TODO
        return

    def stamp_erase(self):
        # TODO
        return

    def tie(self, object):
        # TODO
        return

    def untie(self, object):
        return

    def uphill(self, attribute):
        # TODO
        return

    def uphill4(self, attribute):
        # TODO
        return



def min_one_of(objset, attribute):
    # TODO
    return

def n_of(objset):
    # TODO
    return

def one_of(objset):
    '''
    Returns a random object from within a list of objects

    :param objset:
    :return:
    '''
    # TODO
    return


def constrain_heading(heading):
    # ensure heading stays in the bounds 0-359
    while heading > 359:
        heading -= 360
    while heading < 0:
        heading += 360
    return heading
