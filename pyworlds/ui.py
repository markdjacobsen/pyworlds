# Frame rate: http://stackoverflow.com/questions/28405222/syncing-image-display-with-screen-refresh-rate

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from OpenGL.GL import *
from PyQt5.QtOpenGL import *
from PyQt5.QtWidgets import *
from PIL import Image
import numpy
import sys
import os.path
import time

try:
    import importlib.resources as pkg_resources
except ImportError:
    # Try backported to PY<37 `importlib_resources`.
    import importlib_resources as pkg_resources


class MainWindow(QMainWindow):
    def __init__(self, model):
        super(MainWindow, self).__init__()
        self.model = model
        self.running = False
        self.toolbar = None
        self.menubar = QMenuBar(self)
        self.map_widget = MapWidget(self, self.model)

        self.show_grid = True
        self.grid_color = (0, 100, 0)
        self.grid_thickness = 1
        self.fps = 20

        self.initUI()
        self.show()

    def initUI(self):
        """
        Initialize the user interface for the main window
        """
        self.setGeometry(100, 100, 1000, 800)
        self.setWindowTitle("PyWorlds")

        self.create_actions()
        self.create_toolbar()
        self.create_statusbar()

        self.setCentralWidget(self.map_widget)

        self.gl_timer = QTimer()
        self.gl_timer.timeout.connect(self.on_gl_timer)
        self.gl_timer.start(1000 / self.fps)

    def create_actions(self):
        """
        Create the PyQt actions used by the main window
        """

        self.action_reset = QAction(QIcon(os.path.join(os.path.dirname(__file__), 'art/return.png')), '&Reset', self)
        self.action_reset.setStatusTip('Reset the simulation')
        self.action_reset.setToolTip('Reset the simulation')
        self.action_reset.triggered.connect(self.on_action_reset)

        self.action_tick = QAction(QIcon(os.path.join(os.path.dirname(__file__), 'art/next.png')), '&Tick', self)
        self.action_tick.setStatusTip('Advance a single tick')
        self.action_tick.setToolTip(('Advance a single tick'))
        self.action_tick.triggered.connect(self.on_action_tick)

        self.action_play = QAction(QIcon(os.path.join(os.path.dirname(__file__), 'art/play.png')), '&Play', self)
        self.action_play.setStatusTip('Play/pause simulation')
        self.action_play.setToolTip('Play/pause simulation')
        self.action_play.triggered.connect(self.on_action_play)

        self.action_grid = QAction(QIcon(os.path.join(os.path.dirname(__file__), 'art/settings.png')), '&Grid', self)
        self.action_grid.setStatusTip('Hide/show grid')
        self.action_grid.setToolTip('Hide/show grid')
        self.action_grid.setCheckable(True)
        self.action_grid.setChecked(self.show_grid)
        self.action_grid.triggered.connect(self.on_action_grid)

    def create_toolbar(self):
        """
        Create the toolbar items used by the main window
        """

        #if self.toolbar:
        #    self.removeToolBar(self.toolbar)

        self.toolbar = self.addToolBar('MainToolBar')
        self.toolbar.setObjectName("MainToolBar")

        self.toolbar.addAction(self.action_reset)
        self.toolbar.addAction(self.action_tick)
        self.toolbar.addAction(self.action_play)
        self.toolbar.addAction(self.action_grid)


    def create_statusbar(self):
        """
        Create the status bar used by the main window
        """
        self.tickLabel = QLabel()
        self.tickLabel.setText('Ticks: 0')
        self.statusBar().addPermanentWidget(self.tickLabel)

    def on_action_grid(self):
        self.show_grid = not self.show_grid
        self.update()

    def on_action_reset(self):
        '''
        Called when the user clicks the "Reset" button
        '''

        self.stop_simulation()

        # Reset the simulation model
        self.model.reset()
        self.update()

    def on_action_tick(self):
        '''
        Called when the user clicks the button to advance a single tick.

        '''
        print('on action tick')
        self.model.tick()
        self.update()

    def on_action_play(self):
        if self.running == False:
            self.start_simulation()
        else:
            self.stop_simulation()


    def start_simulation(self):
        self.tick_timer = QTimer()
        self.tick_timer.timeout.connect(self.on_tick_timer)
        self.tick_timer.start(0)

        self.running = True
        self.action_tick.setEnabled(False)
        self.action_play.setIcon(QIcon(os.path.join(os.path.dirname(__file__), 'art/pause.png')))

    def stop_simulation(self):
        self.tick_timer.stop()
        self.action_tick.setEnabled(True)
        self.action_play.setIcon(QIcon(os.path.join(os.path.dirname(__file__), 'art/play.png')))
        self.running = False

    def on_gl_timer(self):
        self.map_widget.updateGL()
        self.update_status_bar()

    def on_tick_timer(self):
        '''
        The timer is used when the user is in play mode and the simulation is advancing.
        This function is called by the timer each time a simulation tick is needed.
        '''
        self.model.tick()

    def update_status_bar(self):
        self.tickLabel.setText("Ticks: " + str(self.model.ticks))


class MapWidget(QGLWidget):
    '''
    A PyQT OpenGL widget for displaying the current frame of a simulation. To use it, you pass a
    simulation object in the constructor. Each time the window is painted, it renders the
    simulation's tilemap and agents
    '''


    def __init__(self, parent, model):
        super(MapWidget, self).__init__()
        self.setAutoBufferSwap(True)
        self.parent = parent
        self.model = model
        self.setMinimumSize(300, 300)
        self.textures_loaded = False

    def load_textures(self):
        self.textures = {}
        self.textures['circle'] = self.loadTexture(os.path.join(os.path.dirname(__file__), 'art/circle.png'))
        self.textures['square'] = self.loadTexture(os.path.join(os.path.dirname(__file__), 'art/square.jpg'))

    def resizeEvent(self, QResizeEvent):
        glViewport(0, 0, self.frameGeometry().width(), self.frameGeometry().height())

    def paintGL(self):

        if self.textures_loaded is False:
            self.load_textures()
            self.textures_loaded = True

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glLoadIdentity()

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0.0, self.frameGeometry().width(), 0.0, self.frameGeometry().height(), 0.0, 1.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        self.draw_tiles()

        self.draw_agents()

    def draw_tiles(self):
        client_height = self.frameGeometry().height()
        scale = self.model.tilesize

        glBegin(GL_QUADS)
        for t in self.model.all_tiles.values():
            glColor3ub(t.color[0], t.color[1], t.color[2])
            self.draw_rect(t.x * scale, client_height - (t.y * scale), scale, scale)
        glEnd()

        if self.parent.show_grid:
            self.draw_grid(self.model.width, self.model.height, self.model.tilesize)

    def draw_agents(self):

        return

    def draw_rect(self, x, y, width, height):
        '''
        Render a rectangle to the GL widget. glBegin() must occur before calling
        this function, and glEnd() must call after
        '''

        glVertex2f(x, y)  # bottom left point
        glVertex2f(x + width, y)  # bottom right point
        glVertex2f(x + width, y - height)  # top right point
        glVertex2f(x, y - height)  # top left point


    def draw_border(self, x, y, width, height):

        glVertex2d(x, y)
        glVertex2d(x + width, y)

        glVertex2d(x + width, y)
        glVertex2d(x + width, y - height)

        glVertex2d(x + width, y - height)
        glVertex2d(x, y - height)

        glVertex2d(x, y - height)
        glVertex2d(x, y)

    def draw_grid(self, width, height, spacing):

        client_height = self.frameGeometry().height()
        maxx = width * spacing
        maxy = client_height - height * spacing

        rgb = self.parent.grid_color
        glColor3ub(rgb[0], rgb[1], rgb[2])
        glLineWidth(self.parent.grid_thickness)

        # Draw horizontal lines
        glBegin(GL_LINES)
        for y in range(0, height+1):
            glVertex2d(0, client_height - y*spacing)
            glVertex2d(maxx, client_height - y*spacing)
        glEnd()

        # Draw vertical lines
        glBegin(GL_LINES)
        for x in range(0, width+1):
            glVertex2d(spacing * x, client_height)
            glVertex2d(spacing * x, maxy)
        glEnd()

    def loadTexture(self, name):

        img = Image.open(name)  # .jpg, .bmp, etc. also work
        img_data = numpy.array(list(img.getdata()), numpy.int8)
        id = glGenTextures(1)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glBindTexture(GL_TEXTURE_2D, id)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.size[0], img.size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, img_data)
        return id



    def drawTexture(self, texture_id, x, y, width, height, colormask='#FFFFFFFF'):
        glColor3f(1, 1, 1)
        glEnable(GL_TEXTURE_2D)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); #-- added for blending
        glBindTexture(GL_TEXTURE_2D, texture_id)
        glColor4fv(self.hex_to_4fv(colormask)) # added for blending

        glBegin(GL_QUADS)  # start drawing a rectangle
        glTexCoord2f(0.0, 0.0)
        glVertex2f(x, y)  # bottom left point
        glTexCoord2f(1.0, 0.0)
        glVertex2f(x + width, y)  # bottom right point
        glTexCoord2f(1.0, 1.0)
        glVertex2f(x + width, y - height)  # top right point
        glTexCoord2f(0.0, 1.0)
        glVertex2f(x, y - height)  # top left point
        glEnd()
        glDisable(GL_TEXTURE_2D)




    def hex_to_rgb(self,value):
        '''
        Convert a hex color value #RRGGBB to a (R,G,B) integer tuple
        '''
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))

    def hex_to_4fv(self,value):
        '''
        Convert a hex color value #RRGGBBTT to a (R,G,B,T) integer tuple
        '''
        value = value.lstrip('#')
        lv = len(value)
        t = tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))
        return tuple(float(x) / 256.0 for x in t)


class SimulationThread(QThread):

    def __init__(self, model):
        super(QThread, self).__init__()
        self.model = model

    def run(self):
        while True:
            self.model.tick()


def run_simulation(model):
    # Store a reference to the simulation

    # Run the simulation's setup function
    model.reset()

    # Create and execute the PyQt application
    app = QApplication([])
    app.setApplicationName("PyWorlds")
    # app.setWindowIcon(QIcon(filename))
    window = MainWindow(model)
    sys.exit(app.exec_())


